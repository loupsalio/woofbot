import { ApplicationCommandType } from "discord.js";
import CommandInterface from "./CommandInterface";

export const list: Array<CommandInterface> = [
  {
    name: "woof",
    description: "Say woof",
    type: ApplicationCommandType.ChatInput,
    options: [
      { required: true, name: "num", type: 4, description: "Number of woof" },
    ],
    exec: (c, i) => {
      const num: any = i.options.get("num")?.value;
      i.followUp("WooF ".repeat(num) + ` ${i.user.username}`);
    },
  },
];
