import { Client, Interaction, GatewayIntentBits } from "discord.js";
import { list } from "./CommandList";
import MessageProcess from "./MessageProcess";
require("dotenv").config();

console.log("AdminWolf is starting...");

const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
  ],
});

client.on("ready", async () => {
  if (!client.user || !client.application) return;
  await client.application?.commands.set(list);
  list.forEach((e) => console.log(`- ${e.name} command loaded`));
  console.log(client.user.username + " is ready !");
});

client.on("interactionCreate", async (interaction: Interaction) => {
  if (interaction.isCommand()) {
    const slashCommand = list.find((c) => c.name === interaction.commandName);
    if (!slashCommand) {
      interaction.followUp({ content: "An error has occurred" });
      return;
    }

    await interaction.deferReply();

    slashCommand.exec(client, interaction);
  }
});

MessageProcess(client);

client.login(process.env.TOKEN);
