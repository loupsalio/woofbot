import { Client, Message } from "discord.js";

export default function MessageProcess(client: Client) {
  const { Configuration, OpenAIApi } = require("openai");
  const configuration = new Configuration({
    apiKey: process.env.API_SECRET,
  });
  const openai = new OpenAIApi(configuration);

  let prompt = "";

  client.on("messageCreate", (message: Message) => {
    if (message.author.bot) return;
    prompt += `You: ${message.content}\n`;
    (async () => {
      const gptResponse = await openai.createCompletion({
        model: "text-davinci-002",
        prompt: prompt,
        max_tokens: 60,
        temperature: 0.3,
        top_p: 0.3,
        presence_penalty: 0,
        frequency_penalty: 0.5,
      });
      message.reply(`${gptResponse.data.choices[0].text.substring(5)}`);
      prompt += `${gptResponse.data.choices[0].text}\n`;
    })();
  });
}
