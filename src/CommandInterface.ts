import {
  CommandInteraction,
  ChatInputApplicationCommandData,
  Client,
} from "discord.js";

export default interface CommandInterface
  extends ChatInputApplicationCommandData {
  exec: (client: Client, interaction: CommandInteraction) => void;
}
