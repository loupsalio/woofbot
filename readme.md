# WoofBot

WoofBot is a simple Discord bot, he say woof and answer throught GPT-3 openAI

## Installation

Use any node package manager to install dependencies.

```bash
npm i
---
yarn install
```

## Typescript

You can transpile to javascript using

```bash
npm run dist
```

## Obfuscation

You can obfuscate javascript using

```bash
npm run prod
```

## Start the project

You can start the project with typescript using

```bash
npm start
```

You can start the project transpiled using

```bash
npm run start-dist
```

You can start the project obfuscated using

```bash
npm run start-post
```
